<?php
	class Validate {
		private $errorMessage;
		private $defaultMessage;
		private $data;

		public function __construct() {
			$this->data = new stdClass();
			$this->errorMessage = new stdClass();
			$this->defaultMessage = new stdClass();
			// ここからエラーメッセージ
			$this->defaultMessage->required = " is required!!!";
			$this->defaultMessage->text = " is can not use metacharactor!!!";
			$this->defaultMessage->numlic = " is not numlic!!!";
			$this->defaultMessage->digit = " is not digit!!!";
			$this->defaultMessage->telephone = " is not telephone number!!!";
			$this->defaultMessage->citycode = " is not citycode!!!";
			$this->defaultMessage->name = " is not name!!!";
			$this->defaultMessage->kana = " is not kana!!!";
			$this->defaultMessage->sex = " is not sex!!!";
		}

		public function __destruct() {
		}

		public function set($type,$name,$params,$required = false) {
			if(isset($params) && $params != "") {
				if($this->$type($params)) {
					$this->data->$name = new stdClass();
					$this->data->$name = $params;
				} else {
					$this->errorMessage->$name = $name.$this->defaultMessage->$type;
				}
			} else {
				//required処理
				if(!$required) {
					$this->data->$name = new stdClass();
					$this->data->$name = $params;
				} else {
					$this->errorMessage->$name = $name.$this->defaultMessage->required;
				}
			}
		}

		public function getAll() {
			$res = array();
			foreach($this->data as $key=>$value) {
				$res[$key] = $value;
			}
			return (array)$res;
		}

		public function getErrorMessage() {
			return (array)$this->errorMessage;
		}

		public function is_error() {
			return is_null($this->errorMessage()) ? true : false;
		}

		private function numlic($data) {
			return preg_match("/^[0-9]{1,}$/",$data) ? true : false;
		}

		private function digit($data) {
			return preg_match("/^[0-1]$/",$data) ? true : false;
		}

		private function telephone($data) {
			return preg_match("/^0\d{1,4}-\d{1,4}-\d{4}$/",$data) ? true : false;
		}

		private function citycode($data) {
			return preg_match("/^\d{3}-\d{4}$/",$data) ? true : false;
		}

		private function name($data) {
			return preg_match("/^([ぁ-ん一-龠]|\s|　)$/",$data) ? true : false;
		}

		private function kana($data) {
			return preg_match("/^[ァ-ヾ]+$/u",$data) ? true : false;
		}

		private function sex($data) {
			return preg_match("/^(male|female)$/",$data) ? true : false;
		}

		private function text($data) {
			//半角英数字・ヒラガナ・カタカナ・漢字のみ
			return preg_match("/^([0-9a-zA-Zぁ-んァ-ヴａ-ｚＡ-Ｚ一-龠]|\s|　)*$/",$data) ? true : false;
		}

	}