<?php
	require_once('class.validate.php');
	if(isset($_POST["submit"])) {
		$validate = new Validate;
		//第1引数 タイプ(class.validate.phpのコンストラクタで宣言したもの)
		//第2引数 名前(かぶっちゃだめ！)
		//第3引数 データ
		//第4引数 必須か否か(省略可能)
		//$validate->set(type,name,data,(true or false));
		$validate->set("digit","digit",$_POST["digit"]);
		$validate->set("numlic","numlic",$_POST["numlic"],true);
		$validate->set("citycode","citycode",$_POST["citycode"]);
		$validate->set("sex","sex",@$_POST["sex"],true);
		$validate->set("numlic","age",@$_POST["age"],true);
		$validate->set("text","comment",@$_POST["comment"],false);
		$validate->set("digit","accept",@$_POST["accept"],true);
		$data = $validate->getAll();
		$errorMessage = $validate->getErrorMessage();
	}
?>
<html>
<head>
	<title>class.validate.php</title>
	<link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/flatly/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.css" type="text/css">
	<style>
		.error {
			color:#FF0000;
		}
	</style>
	<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.js" type="text/javascript"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script>
	$(function(){
	<?php if(isset($errorMessage)) { ?>
		<?php foreach($errorMessage as $key=>$value) { ?>
		$("*[name=<?=$key?>]").parent().append('<p class="error"><?=$value?></p>');
		<?php } ?>
	<?php } ?>
	<?php if(isset($data)) { ?>
		<?php foreach($data as $key=>$value) { ?>
		<?php if($value != "" && !is_null($value)) { ?>
		switch($("*[name=<?=$key?>]").attr('type')) {
			case 'radio' :
				$("*[name=<?=$key?>][value=<?=$value?>]").attr("checked", true);
				break;
			case 'checkbox' :
				$("*[name=<?=$key?>]").attr("checked", true);
				break;
			default :
				$("*[name=<?=$key?>]").val('<?=$value?>');
				break;
		}
		<?php } ?>
		<?php } ?>
	<?php } ?>
	});
	</script>
</head>
<body onload="prettyPrint()" style="padding-top:50px;">
	<div class="container">
		<div class="row">
			<?php if(empty($errorMessage) && isset($data)) : ?>
			<div class="alert alert-success">congratulations!!!</div>
			<?php endif;?>
			<form action="test.php" method="post" accept-charset="utf-8">
				<table class="table">
					<tbody>
						<tr>
							<td>input digit<span class="error">(0 or 1)</span></td>
							<td><input type="text" name="digit" value="" placeholder="input digit"></td>
							<td></td>
						</tr>
						<tr>
							<td>input numlic<span class="error">(0 ～)*required</span></td>
							<td><input type="text" name="numlic" value="" placeholder="input numlic"></td>
							<td></td>
						</tr>
						<tr>
							<td>input citycode<span class="error">(example: xxx-xxx)</span></td>
							<td><input type="text" name="citycode" value="" placeholder="input citycode"></td>
							<td></td>
						</tr>
						<tr>
							<td>check sex<span class="error">*required</span></td>
							<td>
								<input type="radio" name="sex" value="male" placeholder="">male<br>
								<input type="radio" name="sex" value="female" placeholder="">female
							</td>
							<td></td>
						</tr>
						<tr>
							<td>select age<span class="error">*required</span></td>
							<td>
								<select name="age">
									<option value></option>
									<option value="10">～19</option>
									<option value="20">20～29</option>
									<option value="30">30～39</option>
									<option value="40">40～49</option>
									<option value="50">50～59</option>
									<option value="60">60～</option>
								</select>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>input comment<span class="error">(text only)</span></td>
							<td>
								<textarea name="comment"></textarea>
							</td>
							<td></td>
						</tr>
						<tr>
							<td><input type="checkbox" name="accept" value="1">accept<span class="error">*required</span></td>
							<td><input type="submit" class="btn btn-primary" name="submit" value="validate"></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<h3 class="alert alert-warning">after validate data</h3>
		<pre class="prettyprint">
<?php if(isset($data)) : var_dump($data); endif; ?>
		</pre>
		<h3 class="alert alert-info">test.php</h3>
		<pre class="prettyprint linenums lang-php">
&lt;?php
	require_once(&#039;class.validate.php&#039;);
	if(isset($_POST[&quot;submit&quot;])) {
		$validate = new Validate;
		//第1引数 タイプ(class.validate.phpのコンストラクタで宣言したもの)
		//第2引数 名前(かぶっちゃだめ！)
		//第3引数 データ
		//第4引数 必須か否か(省略可能)
		//$validate-&gt;set(type,name,data,(true or false));
		$validate-&gt;set(&quot;digit&quot;,&quot;digit&quot;,$_POST[&quot;digit&quot;]);
		$validate-&gt;set(&quot;numlic&quot;,&quot;numlic&quot;,$_POST[&quot;numlic&quot;],true);
		$validate-&gt;set(&quot;citycode&quot;,&quot;citycode&quot;,$_POST[&quot;citycode&quot;]);
		$validate-&gt;set(&quot;sex&quot;,&quot;sex&quot;,@$_POST[&quot;sex&quot;],true);
		$validate-&gt;set(&quot;numlic&quot;,&quot;age&quot;,@$_POST[&quot;age&quot;],true);
		$validate-&gt;set(&quot;text&quot;,&quot;comment&quot;,@$_POST[&quot;comment&quot;],false);
		$validate-&gt;set(&quot;digit&quot;,&quot;accept&quot;,@$_POST[&quot;accept&quot;],true);
		$data = $validate-&gt;getAll();
		$errorMessage = $validate-&gt;getErrorMessage();
	}
?&gt;
		</pre>
		<h3 class="alert alert-info">class file</h3>
		<pre class="prettyprint linenums lang-php">
<?php
$classfile = file_get_contents("class.validate.php");
print htmlentities($classfile);
?>
		</pre>
		<h3 class="alert alert-info">jQuery code Create</h3>
		<pre class="prettyprint linenums lang-php">
$(function(){
	&lt;?php if(isset($errorMessage)) { ?&gt;
		&lt;?php foreach($errorMessage as $key=&gt;$value) { ?&gt;
			$(&quot;*[name=&lt;?=$key?&gt;]&quot;).parent().append(&#039;&lt;p class=&quot;error&quot;&gt;&lt;?=$value?&gt;&lt;/p&gt;&#039;);
		&lt;?php } ?&gt;
	&lt;?php } ?&gt;
	&lt;?php if(isset($data)) { ?&gt;
		&lt;?php foreach($data as $key=&gt;$value) { ?&gt;
			&lt;?php if($value != &quot;&quot; &amp;&amp; !is_null($value)) { ?&gt;
				switch($(&quot;*[name=&lt;?=$key?&gt;]&quot;).attr(&#039;type&#039;)) {
					case &#039;radio&#039; :
						$(&quot;*[name=&lt;?=$key?&gt;][value=&lt;?=$value?&gt;]&quot;).attr(&quot;checked&quot;, true);
						break;
					case &#039;checkbox&#039; :
						$(&quot;*[name=&lt;?=$key?&gt;]&quot;).attr(&quot;checked&quot;, true);
						break;
					default :
						$(&quot;*[name=&lt;?=$key?&gt;]&quot;).val(&#039;&lt;?=$value?&gt;&#039;);
						break;
				}
			&lt;?php } ?&gt;
		&lt;?php } ?&gt;
	&lt;?php } ?&gt;
});
		</pre>
	</div>
</body>
</html>